﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salary
{
    class Bai22CalSalary
    {
        // using recursion
        static int CalSalaryWithRecursion(int salary, int n)
        {
            if (n > 0)
                return CalSalaryWithRecursion(salary + (salary * 10/100), n-1);
            return salary;
        }

        //without recursion
        static int CalSalaryWithoutRecursion (int salary, int n)
        {
            while (n > 0)
            {
                salary += (salary * 10 / 100);
                n--;
            }
            return salary;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter the initial salary: ");
            int salary = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the work duration (year): ");
            int year = Convert.ToInt32(Console.ReadLine());

            int currentSalary1 = CalSalaryWithRecursion(salary, year);
            int currentSalary2 = CalSalaryWithoutRecursion(salary, year);

            Console.WriteLine("After " + year + " years, the salary (calculated in recursion) will be: " + currentSalary1);
            Console.WriteLine("After " + year + " years, the salary (calculated in iteration) will be: " + currentSalary2);

            Console.ReadLine();
        }
    }
}
