﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//upgraded from previous excercises
namespace product
{
    class Product
    {
        public string name;
        public int price;
        public int quality;
        public int categoryID;

        public Product(string productName, int productPrice, int productQuality, int productCategoryId)
        {
            name = productName;
            price = productPrice;
            quality = productQuality;
            categoryID = productCategoryId;
        }
    }
    //apparently i don't have to create a seperate class file :v

    class Bai11SortByPrice
    {
        static List<Product> FindProduct(List<Product> productList, string productName)
        {
            var productByName = new List<Product>();

            for (int count = 0; count < productList.Count; count++)
            {
                if (Equals(productList[count].name, productName))                    //check for the name of the product
                {
                    productByName.Add(productList[count]);
                }
            }
            return productByName;
        }

        static List<Product> FindProductByCatagory(List<Product> productList, int categoryId)
        {
            var productById = new List<Product>();

            for (int count = 0; count < productList.Count; count++)                 //find in each products
            {
                if (Equals(productList[count].categoryID, categoryId))                    //check for the id of the product
                {
                    productById.Add(productList[count]);                          //add found product to productById
                }
            }

            return productById;
        }

        static List<Product> FindProductByPrice(List<Product> productList, int productPrice)
        {
            var productByPrice = new List<Product>();

            for (int count = 0; count < productList.Count; count++)                  //find in each products
            {
                if (Convert.ToInt32(productList[count].price) <= productPrice)        //check for the price of the product
                {
                    productByPrice.Add(productList[count]);                        //add found product to productByPrice
                }
            }

            return productByPrice;
        }

        static List<Product> SortByPrice(List<Product> productList)
        {
            for (int count1 = productList.Count; count1 > 1; count1--)
            {
                for (int count2 = 0; count2 < count1 - 1; count2++)
                {
                    if (productList[count2].price > productList[count2 + 1].price)
                    {
                        (productList[count2], productList[count2 + 1]) = (productList[count2 + 1], productList[count2]);
                    }
                }
            }
            return productList;
        }

        static void Main(string[] args)
        {
            List<Product> listProduct = new List<Product>
            {
                new Product("CPU", 750, 10, 1),
                new Product("RAM", 50, 2, 2),
                new Product("HDD", 70, 1, 2),
                new Product("Main", 400, 3, 1),
                new Product("Keyboard", 30, 8, 4),
                new Product("VGA", 60, 35, 3),
                new Product("Monitor", 120, 28, 2),
                new Product("Case", 120, 28, 5)
            };

            
            //Ask user to enter product name
            Console.WriteLine("Please enter the name of the product:");
            String name = Console.ReadLine();

            //Run the FindProduct function
            List<Product> rightProduct = FindProduct(listProduct, name);


            if (rightProduct != null)
            {
                //Print the information of the product the user want to find
                Console.WriteLine("Found 1 product:");
                Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
                for (int count = 0; count < rightProduct.Count; count++)
                {
                    Console.WriteLine(rightProduct[count].name + "\t" + rightProduct[count].price + "\t" + rightProduct[count].quality + "\t" + rightProduct[count].categoryID);
                }
            }
            else
            {
                //Ask the user to try id search
                Console.WriteLine("The product you find is not on the list. Please try searching by Id.");
            }

            Console.WriteLine("\n");

            //Ask user to enter product name
            Console.WriteLine("Please enter the category ID of the product:");
            int id = Convert.ToInt32(Console.ReadLine());

            List<Product> rightProduct2 = FindProductByCatagory(listProduct, id);


            if (rightProduct2 != null)
            {
                //Print the information of the product the user want to find
                Console.WriteLine("Found " + rightProduct2.Count + " products:");
                Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
                for (int count = 0; count < rightProduct2.Count; count++)
                {
                    Console.WriteLine(rightProduct2[count].name + "\t" + rightProduct2[count].price + "\t" + rightProduct2[count].quality + "\t" + rightProduct2[count].categoryID);
                }
            }
            else
            {
                //Ask the user to try again
                Console.WriteLine("The product you find is not on the list. Please try searching by Id.");
            }


            Console.WriteLine("\n");

            //Ask user to enter product name
            Console.WriteLine("Please enter the price:");
            int price = Convert.ToInt32(Console.ReadLine());

            List<Product> rightProduct3 = FindProductByPrice(listProduct, price);


            if (rightProduct3 != null)
            {
                //Print the information of the product the user want to find
                Console.WriteLine("Found " + rightProduct3.Count + " products with the price lower than " + price + " :");
                Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
                for (int count = 0; count < rightProduct3.Count; count++)
                {
                    Console.WriteLine(rightProduct3[count].name + "\t" + rightProduct3[count].price + "\t" + rightProduct3[count].quality + "\t" + rightProduct3[count].categoryID);
                }
            }
            else
            {
                //Ask the user to try again
                Console.WriteLine("The product you find is not on the list. Please try again. The program will be terminated.");
            }

            Console.WriteLine("\n");

            List<Product> sortedProduct = SortByPrice(listProduct);

            Console.WriteLine("List of products after sorting by price:");
            Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
            for (int count = 0; count < sortedProduct.Count; count++)
            {
                Console.WriteLine(sortedProduct[count].name + "\t" + sortedProduct[count].price + "\t" + sortedProduct[count].quality + "\t" + sortedProduct[count].categoryID);
            }

            Console.ReadLine();
            
        }
    }
}