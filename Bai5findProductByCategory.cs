﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//upgraded from Bai4findProduct
namespace product
{
    class Bai5findProductByCatagory
    {
        static ArrayList FindProduct(ArrayList pList, string pName)
        {
            foreach (ArrayList innerArray in pList)                 //find in each products (inner Arraylists)
            {
                if (Equals(innerArray[0], pName))                    //check for the name of the product (the first element of innerArray)
                {
                    return innerArray;
                }
            }

            return null;
        }

        static ArrayList findProductByCatagory(ArrayList pList, int pId)
        {
            var productById = new ArrayList();

            foreach (ArrayList innerArray in pList)                 //find in each products (inner Arraylists)
            {
                if (Equals(innerArray[3], pId))                    //check for the id of the product (the first element of innerArray)
                {
                    productById.Add(innerArray);                   //add found product to productById
                }
            }

            return productById;
        }

        static void Main(string[] args)
        {
            //Create arraylist of products with name, price, quality, categoryId
            var listProduct = new ArrayList() {
                new ArrayList() {"CPU", 750, 10, 1},
                new ArrayList() {"RAM", 50, 2, 2},
                new ArrayList() {"HDD", 70, 1, 2},
                new ArrayList() {"Main", 400, 3, 1},
                new ArrayList() {"Keyboard", 30, 8, 4},
                new ArrayList() {"VGA", 60, 35, 3},
                new ArrayList() {"Monitor", 120, 28, 2},
                new ArrayList() {"Case", 120, 28, 5}
            };

            //Create category list (not used yet)
            var catagory = new ArrayList() {
                new ArrayList() {1, "Computer"},
                new ArrayList() {2, "Memory"},
                new ArrayList() {3, "Card"},
                new ArrayList() {4, "Accessory" }
            };

            //Ask user to enter product name
            Console.WriteLine("Please enter the name of the product:");
            String name = Console.ReadLine();

            //Run the FindProduct function
            ArrayList rightProduct = FindProduct(listProduct, name);


            if (rightProduct != null)
            {
                //Print the information of the product the user want to find
                Console.WriteLine("Found 1 product:");
                Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
                foreach (object i in rightProduct)
                {
                    Console.Write(i + "\t");
                }
            }
            else
            {
                //Ask the user to try id search
                Console.WriteLine("The product you find is not on the list. Please try searching by Id.");
            }

            Console.WriteLine("\n");

            //Ask user to enter product name
            Console.WriteLine("Please enter the category ID of the product:");
            int id = Convert.ToInt32(Console.ReadLine());

            ArrayList rightProduct2 = findProductByCatagory(listProduct, id);


            if (rightProduct2 != null)
            {
                //Print the information of the product the user want to find
                Console.WriteLine("Found " + rightProduct2.Count + " products:");
                Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
                foreach (ArrayList innerArray in rightProduct2)
                {
                    foreach (object i in innerArray)
                    {
                        Console.Write(i + "\t");
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                //Ask the user to try again
                Console.WriteLine("The product you find is not on the list. Please try again. The program will be terminated.");
            }

            Console.ReadLine();
        }
    }
}

//PS: no exception checking for now.