﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//upgraded from previous excercises
namespace product
{
    class Product
    {
        public string name;
        public int price;
        public int quality;
        public int categoryID;

        public Product(string productName, int productPrice, int productQuality, int productCategoryId)
        {
            name = productName;
            price = productPrice;
            quality = productQuality;
            categoryID = productCategoryId;
        }
    }

    class Category
    {
        public int id;
        public string name;
        
        public Category(int categoryId, string categoryName)
        {
            id = categoryId;
            name = categoryName;
        }
    }

    //apparently i don't have to create a seperate class file :v

    class Bai15MinByPrice
    {
        static Product FindProduct(List<Product> productList, string productName)
        {

            for (int count = 0; count < productList.Count; count++)
            {
                if (Equals(productList[count].name, productName))                    //check for the name of the product
                {
                    return productList[count];
                }
            }
            return null;
        }

        static List<Product> FindProductByCatagory(List<Product> productList, int categoryId)
        {
            var productById = new List<Product>();

            for (int count = 0; count < productList.Count; count++)                 //find in each products
            {
                if (Equals(productList[count].categoryID, categoryId))                    //check for the id of the product
                {
                    productById.Add(productList[count]);                          //add found product to productById
                }
            }

            return productById;
        }

        static List<Product> FindProductByPrice(List<Product> productList, int productPrice)
        {
            var productByPrice = new List<Product>();

            for (int count = 0; count < productList.Count; count++)                  //find in each products
            {
                if (Convert.ToInt32(productList[count].price) <= productPrice)        //check for the price of the product
                {
                    productByPrice.Add(productList[count]);                        //add found product to productByPrice
                }
            }

            return productByPrice;
        }

        static List<Product> SortByPrice(List<Product> productList)
        {
            for (int count1 = productList.Count; count1 > 1; count1--)                                                          
            {
                for (int count2 = 0; count2 < count1 - 1; count2++)
                {
                    if (productList[count2].price > productList[count2 + 1].price) 
                    {
                        (productList[count2], productList[count2 + 1]) = (productList[count2 + 1], productList[count2]);        //swap
                    }
                }
            }
            return productList;
        }

        static List<Product> SortByName(List<Product> productList)
        {
            for (int count = 1; count < productList.Count; count++)
            {
                Product memory = productList[count];
                int InsertPosition = count - 1;

                while(InsertPosition >=0 && productList[InsertPosition].name.Length < memory.name.Length)
                {
                    productList[InsertPosition + 1] = productList[InsertPosition];
                    InsertPosition--;
                }
                productList[InsertPosition + 1] = memory;
            }
            return productList;
        }
        
        static List<(Product, Category)> SortByCatagoryName (List<Product> productList, List<Category> categoryList)
        {
            for (int count = 1; count < categoryList.Count; count++)                                            //sort the category list in alphabet order
            {
                Category memory = categoryList[count];
                int InsertPosition = count - 1;

                while (InsertPosition >= 0 && categoryList[InsertPosition].name.CompareTo(memory.name) > 0)
                {
                    categoryList[InsertPosition + 1] = categoryList[InsertPosition];
                    InsertPosition--;
                }
                categoryList[InsertPosition + 1] = memory;
            }

            List<(Product, Category)> fullSortedProductList = ProductByCatagory(productList, categoryList);

            return fullSortedProductList;
        }

        static List<(Product, Category)> ProductByCatagory(List<Product> productList, List<Category> categoryList)
        {
            List<(Product, Category)> fullProductList = new List<(Product, Category)>();

            for (int countCategory = 0; countCategory < categoryList.Count; countCategory++)                    //sort the product list corresponding to sorted category
            {
                for (int countProduct = 0; countProduct < productList.Count; countProduct++)
                {
                    if (productList[countProduct].categoryID == categoryList[countCategory].id)
                    {
                        fullProductList.Add((productList[countProduct], categoryList[countCategory]));
                    }
                }
            }

            return fullProductList;
        }

        static Product MinByPrice(List<Product> productList)
        {
            List<Product> sortByPrice = SortByPrice(productList);
            return productList[0];
        }
        static void Main(string[] args)
        {
            List<Product> listProduct = new List<Product>
            {
                new Product("CPU", 750, 10, 1),
                new Product("RAM", 50, 2, 2),
                new Product("HDD", 70, 1, 2),
                new Product("Main", 400, 3, 1),
                new Product("Keyboard", 30, 8, 4),
                new Product("VGA", 60, 35, 3),
                new Product("Monitor", 120, 28, 2),
                new Product("Case", 120, 28, 5)
            };

            List<Category> listCategory = new List<Category>
            {
                new Category(1, "Computer"),
                new Category(2, "Memory"),
                new Category(3, "Card"),
                new Category(4, "Accessory"),
                new Category(5, "Case")
            };
            //added 5th category

            //Ask user to enter product name
            Console.WriteLine("Please enter the name of the product:");
            String name = Console.ReadLine();

            //Run the FindProduct function
            Product rightProduct = FindProduct(listProduct, name);


            if (rightProduct != null)
            {
                //Print the information of the product the user want to find
                Console.WriteLine("Found 1 product:");
                Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
                Console.WriteLine(rightProduct.name + "\t" + rightProduct.price + "\t" + rightProduct.quality + "\t" + rightProduct.categoryID);
            }
            else
            {
                //Ask the user to try id search
                Console.WriteLine("The product you find is not on the list. Please try searching by Id.");
            }

            Console.WriteLine("\n");

            //Ask user to enter product catagory ID
            Console.WriteLine("Please enter the category ID of the product:");
            int id = Convert.ToInt32(Console.ReadLine());

            List<Product> rightProduct2 = FindProductByCatagory(listProduct, id);


            if (rightProduct2 != null)
            {
                //Print the information of the product the user want to find
                Console.WriteLine("Found " + rightProduct2.Count + " products:");
                Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
                for (int count = 0; count < rightProduct2.Count; count++)
                {
                    Console.WriteLine(rightProduct2[count].name + "\t" + rightProduct2[count].price + "\t" + rightProduct2[count].quality + "\t" + rightProduct2[count].categoryID);
                }
            }
            else
            {
                //Ask the user to try again
                Console.WriteLine("The product you find is not on the list. Please try searching by Id.");
            }


            Console.WriteLine("\n");

            //Ask user to enter a price
            Console.WriteLine("Please enter the price:");
            int price = Convert.ToInt32(Console.ReadLine());

            List<Product> rightProduct3 = FindProductByPrice(listProduct, price);


            if (rightProduct3 != null)
            {
                //Print the information of the product the user want to find
                Console.WriteLine("Found " + rightProduct3.Count + " products with the price lower than " + price + " :");
                Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
                for (int count = 0; count < rightProduct3.Count; count++)
                {
                    Console.WriteLine(rightProduct3[count].name + "\t" + rightProduct3[count].price + "\t" + rightProduct3[count].quality + "\t" + rightProduct3[count].categoryID);
                }
            }
            else
            {
                //Ask the user to try again
                Console.WriteLine("The product you find is not on the list. Please try again. The program will be terminated.");
            }

            Console.WriteLine("\n");


            List<Product> sortedProductByPrice = SortByPrice(listProduct);


            Console.WriteLine("List of products after sorting by price:");
            Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
            for (int count = 0; count < sortedProductByPrice.Count; count++)
            {
                Console.WriteLine(sortedProductByPrice[count].name + "\t" + sortedProductByPrice[count].price + "\t" + sortedProductByPrice[count].quality + "\t" + sortedProductByPrice[count].categoryID);
            }

            Console.WriteLine("\n");


            List<Product> sortedProductByName = SortByName(listProduct);


            Console.WriteLine("List of products after sorting by name from longest to shortest:");
            Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
            for (int count = 0; count < sortedProductByName.Count; count++)
            {
                Console.WriteLine(sortedProductByName[count].name + "\t" + sortedProductByName[count].price + "\t" + sortedProductByName[count].quality + "\t" + sortedProductByName[count].categoryID);
            }

            
            Console.WriteLine("\n");


            List<(Product, Category)> sortedProductByCategoryName = SortByCatagoryName(listProduct, listCategory);


            Console.WriteLine("List of products after sorting by catagory name in alphabet order:");
            Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
            for (int count = 0; count < sortedProductByCategoryName.Count; count++)
            {
                Console.WriteLine(sortedProductByCategoryName[count].Item1.name + "\t" + sortedProductByCategoryName[count].Item1.price + "\t" + sortedProductByCategoryName[count].Item1.quality + "\t" + sortedProductByCategoryName[count].Item1.categoryID + "\t" + sortedProductByCategoryName[count].Item2.name);
            }

            
            Console.WriteLine("\n");


            List<(Product, Category)> ProductByCategory = ProductByCatagory(listProduct, listCategory);


            Console.WriteLine("List of products after sorting by catagory name:");
            Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
            for (int count = 0; count < ProductByCategory.Count; count++)
            {
                Console.WriteLine(ProductByCategory[count].Item1.name + "\t" + ProductByCategory[count].Item1.price + "\t" + ProductByCategory[count].Item1.quality + "\t" + ProductByCategory[count].Item1.categoryID + "\t" + ProductByCategory[count].Item2.name);
            }

            Console.WriteLine("\n");


            Product MinimumPriceProduct = MinByPrice(listProduct);

            Console.WriteLine("Product with the lowest price:");
            Console.WriteLine("Name\tPrice\tQuality\tCategoryID");
            Console.WriteLine(MinimumPriceProduct.name + "\t" + MinimumPriceProduct.price + "\t" + MinimumPriceProduct.quality + "\t" + MinimumPriceProduct.categoryID);

            Console.ReadLine();
        }
    }
}