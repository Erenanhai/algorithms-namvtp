﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salary
{
    class Bai22CalMonth
    {
        //without recursion
        static int CalMonthWithoutRecursion(double money, double rate)
        {
            int month = 0;
            double initialMoney = money;
            while (money < initialMoney*2)
            {
                month++;
                money += money * (rate/100);
            }
            return month;
        }

        //with recursion
        static int CalMonthWithRecursion(double money, double rate)
        {
            if (money > 0)
            {
                //not yet correct
                return CalMonthWithRecursion(money - money * (rate / 100), rate) + 1;
            }
            else
                return 0;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Enter the amount of money you want to send: ");
            double money = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the rate: ");
            double rate = Convert.ToDouble(Console.ReadLine());

            int month1 = CalMonthWithoutRecursion(money, rate);
            int month2 = CalMonthWithRecursion(money, rate);

            Console.WriteLine("It takes " + month1 + " months... (with iteration)");
            Console.WriteLine("It takes " + month2 + " months... (with recursion)");

            Console.ReadLine();
        }
    }
}
